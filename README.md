# jabberbot

A pluggable bot for Telegram.

### Usage

```bash
$ git clone https://gitlab.com/docmenthol/jabberbot.git
$ cd jabberbot
$ cp doc/config.sample.json doc/config.json
$ vim doc/config.json  # edit your config as needed
$ yarn install
$ yarn start
```

### Requirements (Linux)

These are the dependencies as required on Raspbian. Adjust for your distro. All
of these are for canvas support as used in the Sunny plugin. If you don't want
to use that plugin, just skip this and remove the `canvas` dependency from
`package.json` before installing dependencies.

* libpixman-1-dev
* libcairo2-dev
* libpango1.0-dev (??)
* libpangocairo-1.0.0 (??)

### Requirements (Windows)

Just install.
