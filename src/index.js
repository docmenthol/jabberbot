const Telegraf = require('telegraf')
const { normalizeConfig, normalizeSender, banner } = require('./lib')

const applyPlugins = (bot, config, plugins) => plugins.reduce((bot, p) => {
  banner(`* ${p.name.toLowerCase()} plugin`)
  const nextBot = p.plugin(bot, config, plugins)
  if (!nextBot) throw `Plugin '${p.name.toLowerCase()}' did not give back a bot instance.`
  return nextBot
}, bot)

const readTheConfig = () =>
  process.argv.length > 2
    ? normalizeConfig(require(process.argv[2]))
    : normalizeConfig()

const makeTheBot = config => {
  console.log(`.: jabberbot, aka '${config.name}'\n`)

  if (config.token === null) {
    console.error('.: err: no api token configured')
    return 1
  }

  banner('loading plugins...')

  const plugins = config.plugins.map(p => require(`./plugins/${p}`))
  const bot = new Telegraf(config.token)
  bot._loaded_plugins = []

  // Bind default commands first, because why shouldn't better versions written
  // as plugins be possible? Don't restrict your developers.
  bot.command('start', async ctx => {
    const { firstName, username } = normalizeSender(ctx.from)
    return ctx.reply(`Hello, ${firstName} ${username ? `(aka, ${username})` : ''}`)
  })

  bot.command('help', async ctx => {
    const helpPlugins = plugins.filter(p =>
      (p.shortName && bot._loaded_plugins.includes(p.shortName))
      && p.hasOwnProperty('help')
      && p.help.length > 0)
    const helpSections = helpPlugins.map(p => {
      let parts = p.help.map(p => `/${p.command} ${p.args} - ${p.description}`.replace(/</g, '&lt;').replace(/>/g, '&gt;'))
      return `<strong>${p.name}</strong>\n${parts.join('\n')}`
    })
    const helpText = helpSections.join('\n\n')
    ctx.replyWithHTML(helpText)
  })

  const app = applyPlugins(bot, config, plugins)

  banner('done\n')

  app.on('inline_query', async () => {})

  app.jabberbot = { plugins }

  return app
}

const runTheBot = bot => bot.startPolling()

const bot = () => {
  const config = readTheConfig()
  const bot = makeTheBot(config)
  if (process.env.CI) {
    console.log('Load test completed successfully.')
    return 0
  }
  return runTheBot(bot)
}

bot()
