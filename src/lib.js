const fetch = require('isomorphic-fetch')
const fs = require('fs')
const JSON5 = require('json5')
const path = require('path')

const normalizeConfig = (config = {}) =>
  Object.assign({
    name: 'Unconfigured',
    plugins: [],
    token: null,
    cmdPrefix: '/',
    dataDirectory: '~/.local/share/jabberbot'
  }, config)

const normalizeSender = from => ({
  id: from.id,
  username: from.username,
  firstName: from.first_name,
  isBot: from.is_bot,
  languageCode: from.language_code
})

const parseMessage = ctx => {
  const msg = ctx.message.text.split(' ')
  const command = msg[0]
  const body = msg.slice(1).join(' ')
  const parts = msg.slice(1)

  const messageID = ctx.message.message_id

  const update = ctx.update.message

  const from = {
    username: update.from.username,
    firstName: update.from.first_name
  }

  let replyTo = null
  if (update.reply_to_message) {
    replyTo = {
      from: {
        username: update.reply_to_message.from.username,
        firstName: update.reply_to_message.from.first_name
      },
      body: update.reply_to_message.text
    }
  }

  return { command, body, parts, from, replyTo, messageID }
}

const tryNumber = n =>
  isNaN(parseFloat(n.toString().charAt(n.length - 1)))
    ? null
    : parseFloat(n) === parseInt(n, 10)
      ? parseInt(n, 10)
      : parseFloat(n)

const times = (n, f) => {
  let returns = []
  for (let i = 0; i < n; i++) returns.push(f())
  return returns
}

const jsonFetch = async uri => (await fetch(uri)).json()

const sorted = a => {
  const b = [ ...a ]
  b.sort()
  return b
}

const randomChoice = arr => {
  const index = Math.floor(Math.random() * arr.length)
  return arr[index]
}

const pickName = user => user.firstName ? user.firstName : user.username

const readFile = path => fs.readFileSync(path, { encoding: 'utf-8' })

const readLines = path =>
  readFile(path)
    .split('\n')
    .map(l => l.trim())

const readJson = path => JSON5.parse(readFile(path))

const loader = basepath => fname => readLines(path.join(basepath, 'name', `${fname}.txt`))

const banner = t => console.log(`.: ${t}`)
const subBanner = t => console.log(`.: -- ${t}`)

const isURL = u => {
  try {
    return Boolean(new URL(u))
  } catch (e) {
    return false
  }
}

module.exports = {
  normalizeConfig,
  normalizeSender,
  parseMessage,
  tryNumber,
  times,
  jsonFetch,
  sorted,
  randomChoice,
  pickName,
  readLines,
  readJson,
  loader,
  banner,
  subBanner,
  isURL
}
