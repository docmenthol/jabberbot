const fetch = require('isomorphic-fetch')
const { parseMessage, tryNumber, jsonFetch, sorted } = require('../lib')

const u = (coin, currency) =>
  `https://min-api.cryptocompare.com/data/price?fsym=${coin}&tsyms=${currency}`

const api = {
  get: {
    coin: async (symbol, currency = 'USD') => {
      const resp = await jsonFetch(u(symbol, currency))
      return resp
    }
  }
}

const parseHoldingsCmd = parts => {
  const [ command, coin, amount ] = parts
  if (!['buy', 'sell'].includes(command.toLowerCase()) ||
      coin === undefined ||
      coin.length === 0 ||
      isNaN(parseFloat(amount))) return null
  return [ command.toLowerCase(), coin.toUpperCase(), parseFloat(amount) ]
}

const help = [
  {
    command: 'cc',
    args: '<amount> <from-currency> [to-currency]',
    description: 'Converts <amount> of <from-currency> into USD, or into [to-currency] if specified.'
  },
  {
    command: 'holdings',
    args: '[<buy|sell> <currency> <amount>]',
    description: 'Allows tracking of cryptocurrency holdings. Running this command with no parameters displays your holdings. This command __does not__ actually buy or sell cryptocurrency, the command names are for simulation.'
  }
]

const plugin = (bot, config) => {
  bot.command('cc', async ctx => {
    const msg = parseMessage(ctx)
    if (msg.parts.length < 2) {
      ctx.replyWithHTML(`<strong>Cryptocurrency</strong>\nUsage: /cc &lt;coin> &lt;amount> [conversion currency (default: USD)]`)
      return
    }
    const cc = (msg.parts[0] || '').toUpperCase()
    const amt = tryNumber(msg.parts[1]) || 1
    const cur = (msg.parts[2] || 'USD').toUpperCase()

    if (cc.length === 0) {
      ctx.reply(`Syntax: ${config.cmdPrefix}cc <amount> <crypto> [currency]`)
      return
    }

    const priceData = await api.get.coin(cc, cur)
    const price = priceData[cur] * amt

    ctx.replyWithHTML(`<strong>Cryptocurrency</strong>\n${amt} ${cc} = ${price} ${cur}`)
  })

  bot.command('holdings', async ctx => {
    const msg = parseMessage(ctx)
    if (msg.parts.length === 0) {
      ctx.replyWithHTML('-- Would show your holdings here. --')
    } else {
      const command = parseHoldingsCmd(msg.parts)
      if (command === null) {
        ctx.reply('Malformed command.\nUsage: /holdings [<buy|sell> <coin> <amount>]')
        return
      }
      const [ action, coin, amount ] = command
      switch (action) {
        case 'buy':
          ctx.replyWithHTML(`Would "buy" ${amount} ${coin} and add it to your holdings.`)
          break
        case 'sell':
          ctx.replyWithHTML(`Would "sell" ${amount} ${coin} and remove it from your holdings.`)
          break
        default:
          ctx.replyWithHTML(`I do not know what "${command}" is.`)
      }
    }
  })

  bot._loaded_plugins.push('cryptocurrency')
  return bot
}

module.exports = { name: 'Cryptocurrency', shortName: 'cryptocurrency', plugin, help }
