const { parseMessage, tryNumber, times } = require('../lib')
var droll = require('droll')

const makeResult = r =>
  `\`${r.roll} → ${r.result.rolls.join(' + ')}${r.result.modifier > 0 ? ` + ${r.result.modifier}` : ''} = ${r.result.total}\``

const help = [
  {
    command: 'roll',
    args: '<dice>',
    description: 'Rolls dice using standard dice notation.'
  }
]

const plugin = bot => {
  bot.command('roll', ctx => {
    const msg = parseMessage(ctx)
    const rolls = msg.parts
      .map(r => ({ roll: r, result: droll.roll(r)}))
      .filter(r => r.result)

    if (rolls.length === 0) {
      ctx.reply(`No valid results for: ${msg.body}. Bad input(s)?`)
      return
    }

    const response = rolls.length === 1
      ? makeResult(rolls[0])
      : rolls.map(r => makeResult(r)).reduce((a, b) => `${a}\n • ${b}`, '')

    ctx.reply(`Results: ${response}`, { parse_mode: 'Markdown' })
  })

  bot._loaded_plugins.push('dice')
  return bot
}

module.exports = { name: 'Dice Roller', shortName: 'dice', plugin, help }
