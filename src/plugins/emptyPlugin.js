const { parseMessage } = require('../lib')

const help = [
  {
    command: 'cmd',
    args: '<args>',
    description: 'Just spits back the arguments.'
  }
]

const plugin = bot => {
  bot.command('command', ctx => {
    let msg = parseMessage(ctx)

    let response = msg.body

    ctx.reply(`Args: ${response}`)
  })

  bot._loaded_plugins.push('emptyPlugin')
  return bot
}

module.exports = { name: 'Plugin Name', shortName: 'emptyPlugin', plugin, help }
