const fs = require('fs')
const path = require('path')
const exec = require('child_process').exec
const { parseMessage, chunkMessage } = require('../lib')

const help = [
  {
    command: 'hla',
    args: '<words>',
    description: 'Says the specified text with the Half-Life announcer voice.'
  },
  {
    command: 'hev',
    args: '<words>',
    description: 'Says the specified text with the Half-Life HEV suit voice.'
  },
  {
    command: 'wordlist',
    args: '<hla|hev>',
    description: 'Returns a list of available words for the specified voice.'
  }
]

const getWords = config => {
  let hlaR = fs.readdirSync(path.join(config.dataDirectory, 'hl', 'vox')).map(f => f.split('.')[0])
  let si = Math.floor(hlaR.length / 2)
  let hla = [hlaR.slice(0, si), hlaR.slice(si)]
  let hev = fs.readdirSync(path.join(config.dataDirectory, 'hl', 'fvox')).map(f => f.split('.')[0])

  return { hla, hev }
}

const validWord = words => word => words.includes(word.toLowerCase())

const plugin = (bot, config) => {
  const voiceCacheDirectory = path.join(config.dataDirectory, 'cache')
  const words = getWords(config)
  const validHla = validWord([ ...words.hla[0], ...words.hla[1] ])
  const validHev = validWord(words.hev)

  bot.command('hla', ctx => {
    let msg = parseMessage(ctx)
    let phrase = msg.body.some(validHla)
      ? msg.body.filter(validHla).map(w => `${w}.wav`)
      : null

    if (!message) {
      const bad = msg.body.filter(w => !validHla(w))
      ctx.reply(`Invalid words: ${bad.join(', ')}`)
      return
    }

    const targetFile = path.join(voiceCacheDirectory, `hl-${msg.messageID}.ogg`)

    exec(`sox ${phrase.join(' ')} ${targetFile}`)

    fs.readFile(targetFile, (fileErr, data) => {
      let f = { voice: data }
      ctx.replyWithVoice(f)
    })
  })

  bot.command('hev', ctx => {
    let msg = parseMessage(ctx)
    let phrase = msg.body.some(validHev)
      ? msg.body.filter(validHev).map(w => `${w}.wav`)
      : null

    if (!message) {
      const bad = msg.body.filter(w => !validHev(w))
      ctx.reply(`Invalid words: ${bad.join(', ')}`)
      return
    }

    const targetFile = path.join(voiceCacheDirectory, `message-${msg.messageID}.ogg`)

    exec(`sox ${phrase.join(' ')} ${targetFile}`)

    fs.readFile(targetFile, (fileErr, data) => {
      let f = { voice: data }
      ctx.replyWithVoice(f)
    })
  })

  bot.command('wordlist', ctx => {
    let msg = parseMessage(ctx)
    let parts = msg.parts
    let arg = parts[0] ? parts[0].toLowerCase() : undefined

    if (!arg) {
      ctx.reply('Must supply one of: hla, hev')
    }

    if (arg === 'hla') {
      words.hla.forEach(l => {
        ctx.reply(l.join(', '))
      })
    } else if (arg === 'hev') {
      ctx.reply(words.hev.join(', '))
    } else {
      ctx.reply(`Invalid wordlist: ${arg}`)
    }
  })

  bot._loaded_plugins.push('hl')
  return bot
}

module.exports = { name: 'Half-Life', shortName: 'hl', plugin, help }
