const fs = require('fs')
const { randomChoice, loader } = require('../lib')

const help = [
  {
    command: 'name',
    args: '',
    description: 'Generates a fancy new name and title.'
  }
]

const generatePersonalName = pieces => {
  let nameA = randomChoice(pieces)
  let nameB
  do {
    nameB = randomChoice(pieces)
  } while (nameB === nameA)
  if (nameA.charAt(nameA.length - 1) === nameB.charAt(0)) {
    nameB =  nameB.slice(1)
  }
  let name = `${nameA}${nameB}`
  return `${name.slice(0,1).toUpperCase()}${name.slice(1)}`
}

const generator = parts => () => {
  const name = generatePersonalName(parts.namePieces)
  const title = randomChoice(parts.titles)
  const noun = randomChoice(parts.nouns)
  const adj = randomChoice(parts.adjectives)
  const group = randomChoice(parts.groups)
  const order = randomChoice(parts.orders)

  return `${name}, ${title} of the ${noun} of the ${adj} ${group}, ${order} of their name`
}

const plugin = (bot, config) => {
  const loadFile = loader(config.dataDirectory)

  const nouns = loadFile('nouns')
  const adjectives = loadFile('adjectives')
  const groups = loadFile('groups')
  const namePieces = loadFile('name-pieces')
  const titles = loadFile('titles')
  const orders = [ 'first', 'second', 'third', 'fourth', 'fifth', 'sixth', 'seventh', 'eighth', 'last' ]
  const generateFullName = generator({ nouns, adjectives, groups, namePieces, titles, orders })

  bot.command('name', ctx => {
    let response = generateFullName()
    ctx.reply(`You are now ${response}.`)
  })

  bot._loaded_plugins.push('name')
  return bot
}

module.exports = { name: 'Name', shortName: 'name', plugin, help }
