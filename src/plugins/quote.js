const path = require('path')
const fs = require('fs')
const { parseMessage, pickName, readJson, randomChoice } = require('../lib')
const JSON5 = require('json5')

const help = [
  {
    command: 'quote',
    args: '',
    description: 'Reply to a message with this command to add it to the quote database. Otherwise, returns a random quote from the database.'
  }
]

const saver = basepath => quotes =>
  fs.writeFileSync(
    path.join(basepath, 'quotes.json5'),
    JSON5.stringify(quotes, null, 2),
    { encoding: 'utf-8' })

const plugin = (bot, config) => {
  const saveQuotes = saver(config.dataDirectory)
  let quotes = readJson(path.join(config.dataDirectory, 'quotes.json5'))
  bot.on('text', ctx => {
    const msg = parseMessage(ctx)
    if (msg.command === '/quote') {
      let response
      if (msg.replyTo) {
        const quotee = pickName(msg.replyTo.from)
        const quoter = pickName(msg.from)
        quotes = [
          ...quotes,
          {
            quote: msg.replyTo.body,
            quotee,
            quoter
          }
        ]
        saveQuotes(quotes)
        response = 'Quote added.'
      } else {
        let choice = randomChoice(quotes)
        response = `"${choice.quote}"\n  -- ${choice.quotee} (Added by ${choice.quoter})`
      }

      ctx.reply(response)
    }
  })

  bot._loaded_plugins.push('quote')
  return bot
}

module.exports = { name: 'Quote', shortName: 'quote', plugin, help }
