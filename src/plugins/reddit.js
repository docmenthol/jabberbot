const fetch = require('isomorphic-fetch')
const { parseMessage, isURL } = require('../lib')

const help = [
  {
    command: 'reddit',
    args: '<link>',
    description: 'Sends back the media from the supplied link in a standard Telegram media message.'
  }
]

const plugin = bot => {
  bot.command('reddit', ctx => {
    let msg = parseMessage(ctx)
    let inURL = msg.body

    if (!isURL(inURL)) {
      ctx.reply('Not a valid URL.')
      return
    }

    let uri = new URL(inURL)
    if (uri.host !== 'reddit.com' && uri.host !== 'old.reddit.com') {
      ctx.reply('Not a reddit URL.')
      return
    }

    let jsonURI = uri.toString() + '.json'
    fetch(jsonURI)
      .then(resp => resp.json())
      .then(json => {
        let postData = json[0].data.children[0].data
        let { post_hint, url } = postData
        switch (post_hint) {
          case 'image':
            ctx.reply(url)
            break
          case 'hosted:video':
            ctx.reply(postData.media.reddit_video.fallback_url)
            break
          default:
            ctx.reply('Don\'t know how to parse this URL. See console.')
            console.dir(postData)
            break
        }
      })

  })

  bot._loaded_plugins.push('reddit')
  return bot
}

module.exports = { name: 'Reddit Media Capture', shortName: 'reddit', plugin, help }
