const fs = require('fs')
const path = require('path')
const { parseMessage, subBanner } = require('../lib')

const help = [
  {
    command: 'consoles',
    args: '',
    description: 'Lists all available consoles.'
  },
  {
    command: 'romsearch',
    args: '<console> <query>',
    description: 'Searches for roms for a given console that match the given query.'
  }
]

const plugin = (bot, config) => {
  const cfg = config.pluginConfig.roms || {}
  const baseDirectory = cfg.baseDirectory && fs.existsSync(cfg.baseDirectory)
    ? cfg.baseDirectory
    : null

  if (!baseDirectory) {
    subBanner('ROM directory does not exist. Skipping plugin.')
    return bot
  }

  const consoles = fs.readdirSync(baseDirectory)
    .filter(f => fs.lstatSync(path.join(baseDirectory, f)).isDirectory())

  bot.command('consoles', ctx => {
    if (!baseDirectory) {
      ctx.reply('[Error] No base directory has been defined. Roms plugin will not function.')
      return
    }

    ctx.reply(`Available consoles: ${consoles.join(', ')}`)
  })

  bot.command('romsearch', ctx => {
    if (!baseDirectory) {
      ctx.reply('[Error] No base directory has been defined. Roms plugin will not function.')
      return
    }

    let msg = parseMessage(ctx)
    let [ gameConsole, query ] = msg.parts

    if (!gameConsole || !query) {
      ctx.reply('A console and query must be supplied.')
      return
    }

    let targetDirectory = path.join(baseDirectory, gameConsole)
    fs.readdir(targetDirectory, (err, files) => {
      let q = query.toLowerCase()
      let roms = files.filter(f => f.toLowerCase().includes(q)).map(f => `* ${f}`)
      ctx.reply(`Found the following roms:\n${roms.join('\n')}`)
    })
  })

  bot.command('romdownload', ctx => {
    if (!baseDirectory) {
      ctx.reply('[Error] No base directory has been defined. Roms plugin will not function.')
      return
    }

    const msg = parseMessage(ctx)
    const [ gameConsole, ...filename ] = msg.parts

    if (console.length === 0 || filename.length === 0) {
      ctx.reply('A console and filename must be supplied.')
      return
    }

    let targetDirectory = path.join(baseDirectory, gameConsole)
    fs.readdir(targetDirectory, (err, files) => {
      let q = filename.join(' ').toLowerCase()
      let rom = files.find(f => f.toLowerCase() === q)
      if (rom) {
        fs.readFile(path.join(targetDirectory, rom), (fileErr, data) => {
          let f = { source: data, filename: rom }
          ctx.replyWithDocument(f)
        })
      } else {
        ctx.reply('Rom not found. Remember, filenames must be exact.')
      }
    })
  })

  bot._loaded_plugins.push('roms')
  return bot
}

module.exports = { name: 'Rom Server', shortName: 'roms', plugin, help }
