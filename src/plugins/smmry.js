const fetch = require('isomorphic-fetch')
const { parseMessage, subBanner } = require('../lib')

const sApi = apiKey => async text =>
  fetch(`https://api.smmry.com/?SM_API_KEY=${apiKey}`, {
    method: 'POST',
    body: `{"sm_api_input":"${text}"}`
  })

const help = [
  {
    command: 'smmry',
    args: '<text>',
    description: 'Summarizes a chunk of text.'
  }
]

const plugin = (bot, config) => {
  const cfg = config.pluginConfig.smmry || {}
  const apiKey = cfg.apiKey || null

  if (!apiKey || apiKey.length !== 10) {
    subBanner('Invalid SMMRY API key. Skipping plugin.')
    return bot
  }

  const api = sApi(apiKey)

  bot.command('smmry', async ctx => {
    // const msg = parseMessage(ctx)
    console.log('Ping!')
  })

  bot._loaded_plugins.push('smmry')
  return bot
}

module.exports = { name: 'Smmry', shortName: 'smmry', plugin, help }
