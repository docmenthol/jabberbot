const fs = require('fs')
const path = require('path')
const stream = require('stream')
const { createCanvas, registerFont } = require('canvas')
const { parseMessage } = require('../lib')

const ImgStream = () => {
  let ws = new stream
  ws.writable = true
  ws.bytes = 0
  ws.write = buf => {
    ws.bytes += buf.length
  }
  ws.end = buf => {
    if (buf) ws.write(buf)
    ws.writable = false
  }
  return ws
}

const help = [
  {
    command: 'sunny',
    args: '<caption>',
    description: 'Generates an Always Sunny title card.'
  }
]

const wrap = (ctx, maxWidth, text) => {
    let words = text.split(' ')
    let lines = []
    let currentLine = words[0]

    for (let i = 1; i < words.length; i++) {
        let word = words[i]
        let width = ctx.measureText(currentLine + ' ' + word).width
        if (width < maxWidth) {
            currentLine += ' ' + word
        } else {
            lines.push(currentLine)
            currentLine = word
        }
    }

    lines.push(currentLine)
    return lines
}

const plugin = (bot, config) => {
  registerFont(`${config.dataDirectory}/Textile.ttf`, { family: 'Textile' })

  bot.command('sunny', ctx => {
    let msg = parseMessage(ctx)
    let caption = msg.body

    let canvas = createCanvas(640, 480)
    let [ cx, cy ] = [canvas.width / 2, canvas.height / 2]
    let ictx = canvas.getContext('2d')
    ictx.font = '24px "Textile"'

    let lines = wrap(ictx, canvas.width - 250, caption)
    if (lines.length > 2) {
      ctx.reply(`Keep it to 2 lines, ${Math.random() < 0.5 ? 'jabroni' : 'bozo'}.`)
      return
    }

    ictx.fillStyle = 'black'
    ictx.fillRect(0, 0, canvas.width, canvas.height)

    ictx.fillStyle = 'white'
    ictx.textBaseline = 'middle'
    ictx.textAlign = 'center'

    if (lines.length === 2) {
      ictx.fillText(lines[0], cx, cy - 20)
      ictx.fillText(lines[1], cx, cy + 20)
    } else {
      ictx.fillText(lines[0], cx, cy)
    }

    let source = canvas.createPNGStream()
    let f = { source, filename: caption }
    ctx.replyWithPhoto(f)
  })

  bot._loaded_plugins.push('sunny')
  return bot
}

module.exports = { name: 'Sunny', shortName: 'sunny', plugin, help }
