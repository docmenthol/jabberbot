const fetch = require('isomorphic-fetch')
const { parseMessage, tryNumber, jsonFetch, subBanner } = require('../lib')

const u = (apiKey, func, loc) =>
  `http://api.wunderground.com/api/${apiKey}/${func}/q/${loc}.json`

const wApi = apiKey => ({
  get: {
    weather: async location => {
      const resp = await jsonFetch(u(apiKey, 'conditions', location))
      const current = resp.current_observation
      return current
    },
    forecast: async location => {
      const resp = await jsonFetch(u(apiKey, 'forecast', location))
      const current = resp.forecast
      return current
    },
    location: async zip => {
      const resp = await jsonFetch(u(apiKey, 'geolookup', zip))
      const location = resp.location
      return location
    }
  }
})

const help = [
  {
    command: 'weather',
    args: '<zip>',
    description: 'Gives the current weather in <zip>.'
  },
  {
    command: 'forecast',
    args: '<zip>',
    description: 'Gives a seven day forecast for <zip>.'
  }
]

const plugin = (bot, config) => {
  const cfg = config.pluginConfig.weather || {}
  const apiKey = cfg.apiKey || null

  if (apiKey === null || apiKey.length !== 16) {
    subBanner('Invalid/missing Weather Underground API key. Skipping plugin.')
    return bot
  }

  const api = wApi(apiKey)

  bot.command('weather', async ctx => {
    const msg = parseMessage(ctx)
    const zip = tryNumber(msg.parts[0])

    if (zip === null) {
      ctx.reply(`Syntax: ${config.cmdPrefix}weather <zip>`)
      return
    }

    const loc = await api.get.location(zip)
    const locString = loc.requesturl.replace('.html', '')
    const weather = await api.get.weather(locString)

    const reply = `<strong>${weather.display_location.full}</strong>
${weather.observation_location.city}
${weather.weather}, ${weather.temp_f}°F (${weather.temp_c}°C), feels like ${weather.feelslike_f}°F (${weather.feelslike_c}°C)
Wind ${weather.wind_string.toLowerCase()}, ${weather.wind_dir.toLowerCase()} at ${weather.wind_mph}mph (${weather.wind_kph}kph)`

    ctx.replyWithHTML(reply)
  })

  bot.command('forecast', async ctx => {
    const msg = parseMessage(ctx)
    const zip = tryNumber(msg.parts[0])

    if (zip === null) {
      ctx.reply(`Syntax: ${config.cmdPrefix}forecast <zip>`)
      return
    }

    const loc = await api.get.location(zip)
    const locString = loc.requesturl.replace('.html', '')
    const forecast = await api.get.forecast(locString)
    const combinedForecast = forecast.txt_forecast.forecastday.map(f =>
      `${f.title}\n${f.fcttext}`).join('\n\n')

    const reply = `<strong>${loc.city}, ${loc.state}</strong>\n\n${combinedForecast}`

    ctx.replyWithHTML(reply)
  })

  bot._loaded_plugins.push('weather')
  return bot
}

module.exports = { name: 'Weather', shortName: 'weather', plugin, help }
